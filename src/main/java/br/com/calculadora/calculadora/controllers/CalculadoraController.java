package br.com.calculadora.calculadora.controllers;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import br.com.calculadora.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora") //request generico
public class CalculadoraController {

    //fazendo o request especifico para o método, o ideal é ser genérico
//    @RequestMapping("/")  //endPoint para fazer a chamada da aplicação
//    public String olaMundo (){
//        return "Ola mundo";
//    }

    @Autowired //instanciando automaticamente
    private CalculadoraService calculadoraService;

    @GetMapping ("/olaMundo") //acessa: http://localhost:8080/calculadora/olaMundo
    public String olaMundo (){
        return "Ola Mundo!";
    }

    @PostMapping("/somar") //sempre que o usuario precisar mandar informação e precisar dessa informação pra processar algo e devolver para o usuario
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        //só vai chamar o método soma se tiver dentro da condição
        //se não tiver nas condições vai ter um bad request - resposta 400
        if(calculadora.getNumeros().size() <=1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário pelo menos 2 números");
        }

        return calculadoraService.somar(calculadora);

    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário pelo menos 2 números");
        }

        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar (@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário pelo menos 2 números");
        }

        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir (@RequestBody Calculadora calculadora){
        //vou sempre aceitar 2 numeros o divisor e dividendo
        if(calculadora.getNumeros().size() != 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário sempre passar 2 números apenas");
        }

        return calculadoraService.dividir(calculadora);
    }
}
