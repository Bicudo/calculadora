package br.com.calculadora.calculadora.models;

import java.util.List;

public class Calculadora {
    private List<Integer> numeros;
    //a classe tem que ta no padrão java Bean (Com construtor vazio e com todos os getters e setters)

    public Calculadora() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
