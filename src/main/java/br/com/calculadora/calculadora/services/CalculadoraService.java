package br.com.calculadora.calculadora.services;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()){
            resultado = resultado +numero;
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtrair (Calculadora calculadora){
        int resultado= calculadora.getNumeros().get(0);

        for(int i=1; i<calculadora.getNumeros().size();i++){
            resultado = resultado - calculadora.getNumeros().get(i);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO multiplicar (Calculadora calculadora){
        int resultado=1;

        for(Integer numero: calculadora.getNumeros()){
            resultado = resultado*numero;
        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO dividir (Calculadora calculadora){
        double quociente;
        int dividendo;
        int divisor;

        dividendo = calculadora.getNumeros().get(0);
        divisor = calculadora.getNumeros().get(1);

        RespostaDTO respostaDTO = new RespostaDTO(dividendo/divisor);
        return respostaDTO;
    }
}
